<?php

return [
    'models' => [
        'group' => 'C4tech\Foundation\Group\Model',
        'role' => 'C4tech\Foundation\Role\Model',
        'user' => 'C4tech\Foundation\User\Model'
    ],

    'repos' => [
        'group' => 'C4tech\Foundation\Group\Repository',
        'role' => 'C4tech\Foundation\Role\Repository',
        'user' => 'C4tech\Foundation\User\Repository'
    ],

    'throttle_login' => true,
    'lockout_time' => 60,
    'max_login_attempts' => 5,

    'remember_local_login' => true,
    'remember_social_login' => true,

    'use_session_login' => true,
    'username_field' => 'email',

    'invalidate_old_tokens' => true,
    'minimum_token_ttl' => 30,
    'token_remember_ttl' => 20160
];
