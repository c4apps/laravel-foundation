# Laravel Foundation

A package for building applications that haver users. Made with love by C4 Tech and Design.

[![Latest Stable Version](https://poser.pugx.org/c4tech/foundation/v/stable)](https://packagist.org/packages/c4tech/foundation)
[![Build Status](https://travis-ci.org/C4Tech/laravel-foundation.svg?branch=master)](https://travis-ci.org/C4Tech/laravel-foundation)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/C4Tech/laravel-foundation/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/C4Tech/laravel-foundation/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/g/C4Tech/laravel-foundation/badges/coverage.png?b=master)](https://scrutinizer-ci.com/g/C4Tech/laravel-foundation/?branch=master)

## Foundational Classes

Many apps start with users, roles, permissions, and some kind of user groups.
We've abstracted out some of the basics of these (e.g. login) into a nice
package so that app development can jump straight into app-specific
features.

### User

Our user model comes ready for action with initial database migrations,
pre-set and configurable relations to roles and groups.

### Role


### Group


### Auth

Sometimes application want OAuth logins; sometimes they don't. Sometimes,
they want both. Our package provides the boilerplate for these in the
Social and Local classes. Social is built on top of Laravel's Socialite.
Go ahead, pick and choose.

In addition to authentication handling, we use JSON Web Tokens for auth
handling instead of the typical session cookie. The JWT class is built on
top of `tymon/jwt-auth`.


## Installation and setup

1. Add `"c4tech/foundation": "1.x"` to your composer requirements and run `composer update`.
2. Add `C4tech\Foundation\ServiceProvider` to `config/app.php` in the 'providers' array.
3. `php artisan vendor:publish`
4. Adjust `config/models.php` and `config/repos.php` to bind your
   extended models and repos to the facades.
5. `php artisan jwt:generate`


### Routes
```
Route::get('auth/login', 'Local@login');
Route::get('auth/logout', 'Local@logout');
Route::get('auth/{provider}', 'Social@redirectTo');
Route::get('auth/{provider}/callback', 'Social@login');
```
