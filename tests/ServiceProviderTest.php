<?php namespace C4tech\Test\Foundation;

use C4tech\Foundation\Contracts\GroupInterface;
use C4tech\Foundation\Contracts\RoleInterface;
use C4tech\Foundation\Contracts\UserInterface;
use C4tech\Foundation\Group\Facade as Group;
use C4tech\Foundation\Role\Facade as Role;
use C4tech\Foundation\User\Facade as User;
use C4tech\Support\Test\Base as TestCase;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Mockery;

class FoundationServiceProviderTest extends TestCase
{
    public function setUp()
    {
        $this->provider = Mockery::mock('C4tech\Foundation\ServiceProvider', [null])
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();
    }

    public function tearDown()
    {
        App::clearResolvedInstances();
        Config::clearResolvedInstances();
        Group::clearResolvedInstances();
        Role::clearResolvedInstances();
        User::clearResolvedInstances();
        parent::tearDown();
    }

    public function testConstructor()
    {
        $property = $this->getProperty($this->provider, 'configPath');
        expect($property->getValue($this->provider))
            ->contains('/resources/config.php');
    }

    public function testBoot()
    {
        include_once('helpers.php');
        $this->provider->shouldReceive('publishes')
            ->with(Mockery::on(function ($configMapping) {
                $keys = array_keys($configMapping);

                $key = array_pop($keys);
                expect($key)->contains('/resources/config.php');

                $value = array_pop($configMapping);
                expect($value)->equals('test/foundation.php');

                return true;
            }))->once();

        Group::shouldReceive('boot')->once();
        Role::shouldReceive('boot')->once();
        User::shouldReceive('boot')->once();

        expect_not($this->provider->boot());
    }

    public function testRegister()
    {
        $this->provider->shouldReceive('mergeConfigFrom')
            ->with(Mockery::type('string'), 'foundation')
            ->once();

        Config::shouldReceive('get')
            ->with('foundation.repos.group', 'C4tech\Foundation\Group\Repository')
            ->once()
            ->andReturn('C4tech\Foundation\Group\Repository');

        Config::shouldReceive('get')
            ->with('foundation.models.group', 'foundation.models.group')
            ->twice()
            ->andReturn('C4tech\Foundation\Group\Model');

        Config::shouldReceive('get')
            ->with('foundation.repos.role', 'C4tech\Foundation\Role\Repository')
            ->once()
            ->andReturn('C4tech\Foundation\Role\Repository');

        Config::shouldReceive('get')
            ->with('foundation.models.role', 'foundation.models.role')
            ->twice()
            ->andReturn('C4tech\Foundation\Role\Model');

        Config::shouldReceive('get')
            ->with('foundation.repos.user', 'C4tech\Foundation\User\Repository')
            ->once()
            ->andReturn('C4tech\Foundation\User\Repository');

        Config::shouldReceive('get')
            ->with('foundation.models.user', 'foundation.models.user')
            ->twice()
            ->andReturn('C4tech\Foundation\User\Model');

        App::shouldReceive('singleton')
            ->with(
                'c4tech.group',
                Mockery::on(function ($closure) {
                    $result = $closure();
                    expect_that($result);
                    expect($result instanceof GroupInterface)->true();
                    return true;
                })
            )->once();

        App::shouldReceive('singleton')
            ->with(
                'c4tech.role',
                Mockery::on(function ($closure) {
                    $result = $closure();
                    expect_that($result);
                    expect($result instanceof RoleInterface)->true();
                    return true;
                })
            )->once();

        App::shouldReceive('singleton')
            ->with(
                'c4tech.user',
                Mockery::on(function ($closure) {
                    $result = $closure();
                    expect_that($result);
                    expect($result instanceof UserInterface)->true();
                    return true;
                })
            )->once();

        expect_not($this->provider->register());
    }

    public function testProvides()
    {
        expect($this->provider->provides())
            ->equals(['c4tech.group', 'c4tech.role', 'c4tech.user']);
    }
}
