<?php namespace C4tech\Test\Foundation\User;

use C4tech\Foundation\Group\Facade as Group;
use C4tech\Foundation\Role\Facade as Role;
use C4tech\Support\Test\Repository as TestCase;
use Exception;
use Illuminate\Support\Facades\Config;
use Mockery;
use stdClass;
use Zizaco\Confide\Facade as Confide;

class RepositoryTest extends TestCase
{
    public function setUp()
    {
        $this->setRepository('C4tech\Foundation\User\Repository', 'C4tech\Foundation\User\Model');
    }

    public function tearDown()
    {
        Config::clearResolvedInstances();
        Confide::clearResolvedInstances();
        Group::clearResolvedInstances();
        Role::clearResolvedInstances();
        parent::tearDown();
    }

    public function testCreateEmpty()
    {
        $this->stubCreate('foundation.models.user', [], null);

        expect($this->repo->create())->null();
    }

    public function testCreateSuccess()
    {
        $data = ['test' => true];
        $created = Mockery::mock('C4tech\Support\Contracts\ModelInterface');
        $created->exists = true;

        $this->stubCreate('foundation.models.user', $data, $created);

        expect($this->repo->create($data))->equals($created);
    }

    public function testCreateRole()
    {
        $data = ['test' => true];

        $role = Mockery::mock('C4tech\Support\Contracts\ModelInterface');
        $role->name = 'mocked role';
        $role->exists = true;

        Role::shouldReceive('findByName')
            ->with($role->name)
            ->once()
            ->andReturn($role);

        $created_model = Mockery::mock('C4tech\Support\Contracts\ModelInterface');
        $created_model->shouldReceive('attachRole')
            ->with($role)
            ->once();

        $created = Mockery::mock('stdClass');
        $created->exists = true;
        $created->object = $created_model;

        $this->stubCreate('foundation.models.user', $data, $created);

        $data['role'] = $role->name;
        expect($this->repo->create($data))->equals($created);
    }

    public function testGroups()
    {
        $tags = ['tags'];
        $this->mocked_model->shouldReceive('groups->cacheTags->remember')
            ->withNoArgs()
            ->with($tags)
            ->with(Mockery::type('integer'))
            ->andReturn(true);

        $this->repo->shouldReceive('getTags')
            ->with('groups')
            ->once()
            ->andReturn($tags);

        expect($this->repo->groups())->true();
    }

    public function testGetGroupsEmpty()
    {
        $collection = Mockery::mock('stdClass');
        $collection->shouldReceive('count')
            ->withNoArgs()
            ->once()
            ->andReturn(0);

        $this->repo->shouldReceive('groups->get')
            ->withNoArgs()
            ->once()
            ->andReturn($collection);

        expect($this->repo->getGroups())->equals($collection);
    }

    public function testGetGroupsItems()
    {
        $new_collection = true;

        $collection = Mockery::mock('stdClass');
        $collection->shouldReceive('count')
            ->withNoArgs()
            ->once()
            ->andReturn(10);

        $collection->shouldReceive('map')
            ->with(
                Mockery::on(function ($callback) {
                    $group = 'test group';
                    Group::shouldReceive('make')
                        ->with($group)
                        ->once()
                        ->andReturn(true);
                    expect($callback($group))->true();

                    return true;
                })
            )
            ->once()
            ->andReturn($new_collection);

        $this->repo->shouldReceive('groups->get')
            ->withNoArgs()
            ->once()
            ->andReturn($collection);

        expect($this->repo->getGroups())->equals($new_collection);
    }

    public function testRoles()
    {
        $tags = ['tags'];
        $this->mocked_model->shouldReceive('roles->cacheTags->remember')
            ->withNoArgs()
            ->with($tags)
            ->with(Mockery::type('integer'))
            ->andReturn(true);

        $this->repo->shouldReceive('getTags')
            ->with('roles')
            ->once()
            ->andReturn($tags);

        expect($this->repo->roles())->true();
    }

    public function testGetRolesEmpty()
    {
        $collection = Mockery::mock('stdClass');
        $collection->shouldReceive('count')
            ->withNoArgs()
            ->once()
            ->andReturn(0);

        $this->repo->shouldReceive('roles->get')
            ->withNoArgs()
            ->once()
            ->andReturn($collection);

        expect($this->repo->getRoles())->equals($collection);
    }

    public function testGetRolesItems()
    {
        $new_collection = true;

        $collection = Mockery::mock('stdClass');
        $collection->shouldReceive('count')
            ->withNoArgs()
            ->once()
            ->andReturn(10);

        $collection->shouldReceive('map')
            ->with(
                Mockery::on(function ($callback) {
                    $role = 'test role';
                    Role::shouldReceive('make')
                        ->with($role)
                        ->once()
                        ->andReturn(true);
                    expect($callback($role))->true();

                    return true;
                })
            )
            ->once()
            ->andReturn($new_collection);

        $this->repo->shouldReceive('roles->get')
            ->withNoArgs()
            ->once()
            ->andReturn($collection);

        expect($this->repo->getRoles())->equals($new_collection);
    }

    public function testCurrentNone()
    {
        Confide::shouldReceive('user')
            ->withNoArgs()
            ->once()
            ->andReturn(null);

        expect($this->repo->current())->null();
    }

    public function testCurrentDoesntExist()
    {
        $user = new stdClass;
        $user->exists = false;

        Confide::shouldReceive('user')
            ->withNoArgs()
            ->once()
            ->andReturn($user);

        expect($this->repo->current())->null();
    }

    public function testCurrentCached()
    {
        $user = new stdClass;
        $user->exists = true;
        $user->id = $user_id = 13;

        $instances = [$user_id => $user];
        $this->setPropertyValue($this->repo, 'instances', $instances, true);

        Confide::shouldReceive('user')
            ->withNoArgs()
            ->once()
            ->andReturn($user);

        expect($this->repo->current())->equals($user);
    }

    public function testCurrentNeedsCache()
    {
        $user = Mockery::mock('C4tech\Support\Contracts\ModelInterface');
        $user->exists = true;
        $user->id = $user_id = 13;

        $instances = [];
        $this->setPropertyValue($this->repo, 'instances', $instances, true);

        Confide::shouldReceive('user')
            ->withNoArgs()
            ->once()
            ->andReturn($user);

        $this->repo->shouldReceive('make')
            ->with(
                Mockery::on(function ($model) use ($user, $user_id) {
                    expect($model)->equals($user);

                    $instances = [$user_id => true];
                    $this->setPropertyValue($this->repo, 'instances', $instances, true);

                    return true;
                })
            )
            ->once();

        expect($this->repo->current())->true();
    }

    public function testExistsNotConfirmedIsConfirmed()
    {
        $input = 'test';

        $this->mocked_model->shouldReceive('checkUserExists')
            ->with($input)
            ->once()
            ->andReturn(true);

        $this->mocked_model->shouldReceive('isConfirmed')
            ->with($input)
            ->once()
            ->andReturn(true);

        expect($this->repo->existsNotConfirmed($input))->false();
    }

    public function testExistsNotConfirmedDoesntExist()
    {
        $input = 'test';

        $this->mocked_model->shouldReceive('checkUserExists')
            ->with($input)
            ->once()
            ->andReturn(false);

        expect($this->repo->existsNotConfirmed($input))->false();
    }

    public function testExistsNotConfirmedTrue()
    {
        $input = 'test';

        $this->mocked_model->shouldReceive('checkUserExists')
            ->with($input)
            ->once()
            ->andReturn(true);

        $this->mocked_model->shouldReceive('isConfirmed')
            ->with($input)
            ->once()
            ->andReturn(false);

        expect($this->repo->existsNotConfirmed($input))->true();
    }
}
