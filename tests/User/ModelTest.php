<?php namespace C4tech\Test\Foundation\User;

use C4tech\Support\Test\Model as TestCase;
use Illuminate\Support\Facades\Config;
use Mockery;

class ModelTest extends TestCase
{
    public function setUp()
    {
        $this->setModel('C4tech\Foundation\User\Model');
    }

    public function tearDown()
    {
        Config::clearResolvedInstances();
        parent::tearDown();
    }

    public function testGroups()
    {
        $model = 'C4tech\Foundation\Group\Model';
        Config::shouldReceive('get')
            ->with('foundation.models.group', $model)
            ->once()
            ->andReturn($model);

        return $this->verifyBelongsToMany('groups', $model);
    }
}
