<?php namespace C4tech\Test\Foundation\User;

use C4tech\Support\Test\Facade;

class FacadeTest extends Facade
{
    protected $facade = 'C4tech\Foundation\User\Facade';

    public function testFacade()
    {
        $this->verifyFacadeAccessor('c4tech.user');
    }
}
