<?php namespace C4tech\Test\Foundation\Role;

use C4tech\Support\Test\Model as TestCase;
use Illuminate\Support\Facades\Config;
use Mockery;

class ModelTest extends TestCase
{
    public function setUp()
    {
        $this->setModel('C4tech\Foundation\Role\Model');
    }

    public function tearDown()
    {
        Config::clearResolvedInstances();
        parent::tearDown();
    }

    public function testScopeWhereName()
    {
        $name = 'test';

        $query = Mockery::mock('stdClass');
        $query->shouldReceive('where')
            ->with('name', $name)
            ->once()
            ->andReturn(true);

        expect($this->model->scopeWhereName($query, $name))->true();
    }
}
