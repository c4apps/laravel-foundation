<?php namespace C4tech\Test\Foundation\Role;

use C4tech\Foundation\User\Facade as User;
use C4tech\Support\Test\Repository as TestCase;
use Exception;
use Illuminate\Support\Facades\Config;
use Mockery;

class RepositoryTest extends TestCase
{
    public function setUp()
    {
        $this->setRepository('C4tech\Foundation\Role\Repository', 'C4tech\Foundation\Role\Model');
    }

    public function tearDown()
    {
        Config::clearResolvedInstances();
        User::clearResolvedInstances();
        parent::tearDown();
    }

    public function testFindByNameEmpty()
    {
        $name = 'test';
        $this->mocked_model->shouldReceive('whereName->cacheTags->remember->get')
            ->with($name)
            ->with('roles-name-' . $name)
            ->with(Mockery::type('integer'))
            ->withNoArgs()
            ->once()
            ->andReturn(null);

        expect($this->repo->findByName($name))->null();
    }

    public function testFindByNameExists()
    {
        $name = 'test';
        $object = Mockery::mock('C4tech\Support\Contracts\ModelInterface');

        $this->mocked_model->shouldReceive('whereName->cacheTags->remember->get')
            ->with($name)
            ->with('roles-name-' . $name)
            ->with(Mockery::type('integer'))
            ->withNoArgs()
            ->once()
            ->andReturn($object);

        $this->repo->shouldReceive('make')
            ->with($object)
            ->once()
            ->andReturn(true);

        expect($this->repo->findByName($name))->true();
    }

    public function testPerms()
    {
        $tags = ['tags'];
        $this->mocked_model->shouldReceive('perms->cacheTags->remember')
            ->withNoArgs()
            ->with($tags)
            ->with(Mockery::type('integer'))
            ->andReturn(true);

        $this->repo->shouldReceive('getTags')
            ->with('perms')
            ->once()
            ->andReturn($tags);

        expect($this->repo->perms())->true();
    }

    public function testGetPerms()
    {
        $this->repo->shouldReceive('perms->get')
            ->withNoArgs()
            ->once()
            ->andReturn(true);

        expect($this->repo->getPerms())->true();
    }

    public function testUsers()
    {
        $tags = ['tags'];
        $this->mocked_model->shouldReceive('users->cacheTags->remember')
            ->withNoArgs()
            ->with($tags)
            ->with(Mockery::type('integer'))
            ->andReturn(true);

        $this->repo->shouldReceive('getTags')
            ->with('users')
            ->once()
            ->andReturn($tags);

        expect($this->repo->users())->true();
    }

    public function testGetUserEmpty()
    {
        $collection = Mockery::mock('stdClass');
        $collection->shouldReceive('count')
            ->withNoArgs()
            ->once()
            ->andReturn(0);

        $this->repo->shouldReceive('users->get')
            ->withNoArgs()
            ->once()
            ->andReturn($collection);

        expect($this->repo->getUsers())->equals($collection);
    }

    public function testGetUserItems()
    {
        $new_collection = true;

        $collection = Mockery::mock('stdClass');
        $collection->shouldReceive('count')
            ->withNoArgs()
            ->once()
            ->andReturn(10);

        $collection->shouldReceive('map')
            ->with(
                Mockery::on(function ($callback) {
                    $user = 'test user';
                    User::shouldReceive('make')
                        ->with($user)
                        ->once()
                        ->andReturn(true);
                    expect($callback($user))->true();

                    return true;
                })
            )
            ->once()
            ->andReturn($new_collection);

        $this->repo->shouldReceive('users->get')
            ->withNoArgs()
            ->once()
            ->andReturn($collection);

        expect($this->repo->getUsers())->equals($new_collection);
    }
}
