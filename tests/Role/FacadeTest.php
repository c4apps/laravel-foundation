<?php namespace C4tech\Test\Foundation\Role;

use C4tech\Support\Test\Facade;

class FacadeTest extends Facade
{
    protected $facade = 'C4tech\Foundation\Role\Facade';

    public function testFacade()
    {
        $this->verifyFacadeAccessor('c4tech.role');
    }
}
