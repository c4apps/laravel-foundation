<?php namespace C4tech\Test\Foundation\Group;

use C4tech\Foundation\User\Facade as User;
use C4tech\Support\Test\Repository as TestCase;
use Exception;
use Illuminate\Support\Facades\Config;
use Mockery;

class RepositoryTest extends TestCase
{
    public function setUp()
    {
        $this->setRepository('C4tech\Foundation\Group\Repository', 'C4tech\Foundation\Group\Model');
    }

    public function tearDown()
    {
        Config::clearResolvedInstances();
        User::clearResolvedInstances();
        parent::tearDown();
    }

    public function testUsers()
    {
        $this->mocked_model->shouldReceive('users')
            ->withNoArgs()
            ->andReturn(true);

        expect($this->repo->users())->true();
    }

    public function testGetUserEmpty()
    {
        $collection = Mockery::mock('stdClass');
        $collection->shouldReceive('count')
            ->withNoArgs()
            ->once()
            ->andReturn(0);

        $this->repo->shouldReceive('users->get')
            ->withNoArgs()
            ->once()
            ->andReturn($collection);

        expect($this->repo->getUsers())->equals($collection);
    }

    public function testGetUserItems()
    {
        $new_collection = true;

        $collection = Mockery::mock('stdClass');
        $collection->shouldReceive('count')
            ->withNoArgs()
            ->once()
            ->andReturn(10);

        $collection->shouldReceive('map')
            ->with(
                Mockery::on(function ($callback) {
                    $user = 'test user';
                    User::shouldReceive('make')
                        ->with($user)
                        ->once()
                        ->andReturn(true);
                    expect($callback($user))->true();

                    return true;
                })
            )
            ->once()
            ->andReturn($new_collection);

        $this->repo->shouldReceive('users->get')
            ->withNoArgs()
            ->once()
            ->andReturn($collection);

        expect($this->repo->getUsers())->equals($new_collection);
    }
}
