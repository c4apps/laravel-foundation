<?php namespace C4tech\Test\Foundation\Group;

use C4tech\Support\Test\Facade;

class FacadeTest extends Facade
{
    protected $facade = 'C4tech\Foundation\Group\Facade';

    public function testFacade()
    {
        $this->verifyFacadeAccessor('c4tech.group');
    }
}
