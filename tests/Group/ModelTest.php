<?php namespace C4tech\Test\Foundation\Group;

use C4tech\Support\Test\Model as TestCase;
use Illuminate\Support\Facades\Config;

class ModelTest extends TestCase
{
    public function setUp()
    {
        $this->setModel('C4tech\Foundation\Group\Model');
    }

    public function tearDown()
    {
        Config::clearResolvedInstances();
        parent::tearDown();
    }

    public function testForeignKey()
    {
        expect($this->model->getForeignKey())->equals('group_id');
    }

    public function testUsers()
    {
        $model = 'C4tech\Foundation\User\Model';
        Config::shouldReceive('get')
            ->with('foundation.models.user', $model)
            ->once()
            ->andReturn($model);

        return $this->verifyBelongsToMany('users', $model);
    }
}
