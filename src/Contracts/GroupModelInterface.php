<?php namespace C4tech\Foundation\Contracts;

use C4tech\Support\Contracts\ModelInterface;

interface GroupModelInterface extends ModelInterface
{
    public function users();
}
