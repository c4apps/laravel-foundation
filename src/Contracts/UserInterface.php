<?php namespace C4tech\Foundation\Contracts;

use C4tech\Support\Contracts\ResourceInterface;
use Laravel\Socialite\Contracts\User;

interface UserInterface extends ResourceInterface
{
    public function findOrCreateSocial(User $social_user);
}
