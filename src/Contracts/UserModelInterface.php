<?php namespace C4tech\Foundation\Contracts;

use C4tech\Support\Contracts\ModelInterface;

interface UserModelInterface extends ModelInterface
{
    public function groups();
}
