<?php namespace C4tech\Foundation\Auth;

use C4tech\Foundation\User\Facade as User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;

class Social
{
    protected function checkProviderAllowed($provider)
    {
        if (!Config::get('services.' . $provider . '.client_secret', false)) {
            throw new AuthException('The oauth service provider is not configured');
        }
    }

    public function redirectTo($provider)
    {
        $this->checkProviderAllowed($provider);
        return Socialite::with($provider)->redirect();
    }

    public function login($provider)
    {
        $this->checkProviderAllowed($provider);

        $user = User::findOrCreateSocial(Socialite::with($provider)->user());

        if (Config::get('foundation.use_session_login', false)) {
            return $this->sessionLogin($user);
        }

        return $this->tokenLogin($user);
    }

    protected function tokenLogin($user, $remember = true)
    {
        Auth::onceUsingId($user->id);
        Session::set('remember', Config::get('foundation.remember_social_login', $remember));

        return $user;
    }

    protected function sessionLogin($user, $remember = true)
    {
        Auth::login($user, Config::get('foundation.remember_social_login', $remember));

        return $user;
    }
}
