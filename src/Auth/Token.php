<?php namespace C4tech\Foundation\Auth;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class Token
{
    /**
     * [authenticateFromRequest description]
     * @param  [type] $request [description]
     * @return boolean
     */
    public function authenticate($request)
    {
        if ($token = $this->getTokenFromRequest($request)) {
            try {
                return (JWTAuth::authenticate($token));
            } catch (TokenExpiredException $e) {
                // Do nothing
            } catch (JWTException $e) {
                // Do nothing
            }
        }

        return false;
    }

    /**
     * [refresh description]
     * @param  [type] $request [description]
     * @return string|void
     */
    public function refresh($request)
    {
        $expired = false;
        if (!($token = $this->getTokenFromRequest($request))) {
            return;
        }

        try {
            $payload = JWTAuth::getPayload($token);
        } catch (TokenExpiredException $e) {
            if (!($token = $this->createToken())) {
                return;
            }
            $payload = JWTAuth::getPayload($token);
        } catch (JWTException $e) {
            if (!($token = $this->createToken())) {
                return;
            }
            $payload = JWTAuth::getPayload($token);
        }

        $expiration = Carbon::createFromTimeStamp($payload->get('exp'));

        if (Carbon::now()->diffInMinutes($expiration) > Config::get('foundation.minimum_token_ttl')) {
            if (Config::get('foundation.invalidate_old_tokens')) {
                JWTAuth::invalidate($token);
                $expired = true;
            }
        }

        if ($expired) {
            $token = $this->createToken($payload->get('remember'), $payload->get('iat'));
        }

        return $token;
    }

    /**
     * [createToken description]
     * @param  boolean $remember [description]
     * @param  boolean $issued   [description]
     * @return string|void
     */
    public function createToken($remember = false, $issued = false)
    {
        $user = Auth::user();
        if (!$user) {
            return;
        }

        $remember = $remember ?: Session::get('remember', false);

        $factory = JWTFactory::sub($user->id);
        if ($remember) {
            $factory->setTTL(Config::get('foundation.token_remember_ttl', 60*24*14));
        }

        if ($issued) {
            $factory->addClaim('iat', $issued);
        }

        return JWTAuth::encode($factory->make())->get();
    }

    /**
     * [getTokenFromRequest description]
     * @param  [type] $request [description]
     * @return string|void
     */
    protected function getTokenFromRequest($request)
    {
        $token = JWTAuth::setRequest($request)->getToken();

        if ($token) {
            return $token;
        }
    }
}
