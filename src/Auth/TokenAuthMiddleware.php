<?php namespace C4tech\Foundation\Auth;

use Closure;
use Illuminate\Support\Facades\Config;

/**
 * Attempt to log user in from JWT. Continue as guest if login fails.
 */
class TokenAuthMiddleware
{
    protected $jwt = null;

    public function __construct(Token $token)
    {
        $this->jwt = $token;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Config::get('foundation.use_session_login', false)) {
            $this->jwt->authenticate($request);
        }

        return $next($request);
    }
}
