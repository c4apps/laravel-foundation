<?php namespace C4tech\Foundation\Auth;

use Closure;
use Illuminate\Support\Facades\Config;

/**
 * Attempt to refresh a user's token.
 */
class TokenRefreshMiddleware
{
    protected $jwt = null;

    public function __construct(Token $token)
    {
        $this->jwt = $token;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (Config::get('foundation.use_session_login', false)) {
            return $response;
        }

        $token = $this->jwt->refresh($request);

        // Catch initial login
        if (!$token) {
            $token = $this->jwt->createToken();
        }

        if ($token) {
            $response->headers->set('Authorization', 'Bearer ' . $token);
        } else {
            $response->headers->set('Authorization', false);
        }

        return $response;
    }
}
