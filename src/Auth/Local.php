<?php namespace C4tech\Foundation\Auth;

use C4tech\Foundation\User\Facade as User;
use Illuminate\Cache\RateLimiter;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class Local
{
    protected $limiter = null;

    public function __constructor()
    {
        $this->limiter = App::make(RateLimiter::class);
    }

    public function login($username, $password, $remember = null)
    {
        if ($this->hasTooManyAttempts($username)) {
            return;
        }

        $username_field = Config::get('foundation.username_field', 'email');

        $credentials = [
            $username_field => $username,
            'password' => $password
        ];

        if (is_null($remember)) {
            $remember = Config::get('foundation.remember_local_login', false);
        }

        if (Config::get('foundation.use_session_login', false)) {
            $user = $this->sessionLogin($credentials, $remember);
        } else {
            $user = $this->tokenLogin($credentials, $remember);
        }

        if ($user) {
            $this->clearAttempts($username);
            return $user;
        }

        $this->incrementAttempts($username);
    }

    public function tokenLogin($credentials, $remember = true)
    {
        if (Auth::once($credentials)) {
            Session::set('remember', $remember);
            return Auth::user();
        }
    }

    public function sessionLogin($credentials, $remember = true)
    {
        if (Auth::attempt($credentials, $remember)) {
            return Auth::user();
        }
    }

    protected function hasTooManyAttempts($username)
    {
        $throttles = Config::get('foundation.throttle_login');

        if (!$throttles) {
            return false;
        }

        $lockout = Config::get('foundation.lockout_time', 60);

        return $this->limiter->tooManyAttempts(
            $username . Request::ip(),
            Config::get('foundation.max_login_attempts', 5),
            $lockout / 60
        );
    }

    protected function incrementAttempts($username)
    {
        if (Config::get('foundation.throttle_login')) {
            $this->limiter->hit($username . Request::ip());
        }
    }

    protected function clearAttempts($username)
    {
        if (Config::get('foundation.throttle_login')) {
            $this->limiter->clear($username . Request::ip());
        }
    }

    public function logout()
    {
        Auth::logout();
    }
}
