<?php namespace C4tech\Foundation;

use C4tech\Foundation\Group\Facade as Group;
use C4tech\Foundation\Role\Facade as Role;
use C4tech\Foundation\User\Facade as User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    protected $configPath = '';

    protected $migrationPath = '';

    /**
     * @inheritDoc
     */
    public function __construct($app)
    {
        $this->configPath = __DIR__ . '/../resources/config.php';
        $this->migrationPath = __DIR__ . '/../resources/migrations';
        parent::__construct($app);
    }

    /**
     * @inheritDoc
     */
    public function boot()
    {
        $configs = [];
        $configs[$this->configPath] = config_path('foundation.php');
        $this->publishes($configs, 'config');

        $migrations = [];
        $migrations[$this->migrationPath] = database_path('migrations');
        $this->publishes($migrations, 'migrations');

        Group::boot();
        User::boot();
    }

    /**
     * @inheritDoc
     */
    public function register()
    {
        $this->mergeConfigFrom($this->configPath, 'foundation');

        App::singleton(
            'c4tech.group',
            function () {
                $repo = Config::get('foundation.repos.group', 'C4tech\Foundation\Group\Repository');
                return new $repo;
            }
        );

        App::singleton(
            'c4tech.user',
            function () {
                $repo = Config::get('foundation.repos.user', 'C4tech\Foundation\User\Repository');
                return new $repo;
            }
        );
    }

    /**
     * @inheritDoc
     */
    public function provides()
    {
        return ['c4tech.group', 'c4tech.user'];
    }
}
