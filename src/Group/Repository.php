<?php namespace C4tech\Foundation\Group;

use C4tech\Foundation\Contracts\GroupInterface;
use C4tech\Foundation\User\Facade as User;
use C4tech\NestedSet\Repository as BaseRepository;
use Illuminate\Support\Facades\Cache;

/**
 * Group Repository
 *
 * Common business logic for Groups.
 */
class Repository extends BaseRepository implements GroupInterface
{
    /**
     * @inheritDoc
     */
    protected static $model = 'foundation.models.group';

    /**
     * Users
     *
     * Users within this Group.
     */
    public function users()
    {
        return $this->object->users();
    }

    /**
     * Get Users
     *
     * Accessor to cacheable Model query.
     */
    public function getUsers()
    {
        return Cache::tags($this->formatTag('users'))
            ->remember(
                $this->getCacheId('users'),
                self::CACHE_DAY,
                function () {
                    $users = $this->users()->get();

                    if ($users->count()) {
                        $users = $users->map(function ($user) {
                            return User::make($user);
                        });
                    }

                    return $users;
                }
            );
    }
}
