<?php namespace C4tech\Foundation\Group;

use C4tech\Foundation\Contracts\GroupModelInterface;
use C4tech\NestedSet\Model as BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Config;

/**
 * Group Model
 *
 * A Group of Users.
 */
class Model extends BaseModel implements GroupModelInterface
{
    /**
     * Consume soft deleting trait.
     */
    use SoftDeletes;

    /**
     * @inheritdoc
     */
    protected $table = 'groups';

    /**
     * @inheritdoc
     */
    public function getForeignKey()
    {
        return 'group_id';
    }

    /**
     * Users
     *
     * Users within this Group.
     */
    public function users()
    {
        return $this->belongsToMany(Config::get('foundation.models.user', 'C4tech\Foundation\User\Model'));
    }
}
