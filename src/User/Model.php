<?php namespace C4tech\Foundation\User;

use C4tech\Foundation\Contracts\UserModelInterface;
use C4tech\Support\Model as BaseModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class Model extends BaseModel implements
    UserModelInterface,
    AuthenticatableContract,
    CanResetPasswordContract
{
    /**
     * Consume several traits.
     */
    use Authenticatable, CanResetPassword, SoftDeletes;

        /**
     * @inheritDoc
     */
    protected $table = 'users';

    /**
     * @inheritDoc
     */
    protected $guarded = ['id'];

    /**
     * @inheritDoc
     */
    protected $hidden = [
        'password',
        'remember_token',
        'confirmation_code'
    ];

    /**
     * Groups
     *
     * The Groups to which this User belongs.
     */
    public function groups()
    {
        return $this->belongsToMany(Config::get('foundation.models.group', 'C4tech\Foundation\Group\Model'));
    }
}
