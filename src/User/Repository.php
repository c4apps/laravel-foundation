<?php namespace C4tech\Foundation\User;

use C4tech\Foundation\Contracts\UserInterface;
use C4tech\Foundation\Group\Facade as Group;
use C4tech\Foundation\Role\Facade as Role;
use C4tech\Support\Repository as BaseRepository;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Laravel\Socialite\Contracts\User as SocialUser;

/**
 * User Repository
 *
 * A system User.
 */
class Repository extends BaseRepository implements
    UserInterface,
    AuthenticatableContract,
    CanResetPasswordContract
{
    /**
     * @inheritDoc
     */
    protected static $model = 'foundation.models.user';

    /**
     * @inheritDoc
     */
    public function getAuthIdentifierName()
    {
        return $this->object->getKeyName();
    }

    /**
     * @inheritDoc
     */
    public function getAuthIdentifier()
    {
        return $this->object->getAuthIdentifier();
    }

    /**
     * @inheritDoc
     */
    public function getAuthPassword()
    {
        return $this->object->getAuthPassword();
    }

    /**
     * @inheritDoc
     */
    public function getRememberToken()
    {
        return $this->object->getRememberToken();
    }

    /**
     * @inheritDoc
     */
    public function setRememberToken($value)
    {
        $this->object->setRememberToken($value);
    }

    /**
     * @inheritDoc
     */
    public function getRememberTokenName()
    {
        return $this->object->getRememberTokenName();
    }

    /**
     * @inheritDoc
     */
    public function getEmailForPasswordReset()
    {
        return $this->object->getEmailForPasswordReset();
    }

    /**
     * Groups
     *
     * Wrapper to Model query.
     */
    public function groups()
    {
        return $this->object->groups();
    }

    /**
     * Get Groups
     *
     * Accessor to cacheable Model query.
     */
    public function getGroups()
    {
        return Cache::tags($this->formatTag('groups'))
            ->remember(
                $this->getCacheId('groups'),
                self::CACHE_MONTH,
                function () {
                    $groups = $this->groups()->get();

                    return ($groups instanceof Collection) ?
                        Group::makeCollection($groups) :
                        Group::make($groups);
                }
            );
    }

    /**
     * Current
     *
     * Shorthand accessor to get the currently logged in User.
     */
    public function current()
    {
        if (!($user = Auth::user())) {
            return;
        }

        if (!$user->exists) {
            return;
        }

        $key = $this->formatTag($user->id);

        if (!isset(static::$instances[$key])) {
            $this->make($user);
        }

        return static::$instances[$key];
    }

    /**
     * Exists Not Confirmed
     *
     * Wrapper to Model queries to determine if the input matches an unconfirmed
     * user.
     */
    public function existsNotConfirmed($input)
    {
        $result = false;
        if ($user = $this->find($input)) {
            $result = ($user->exists && isset($user->confirmation_code));
        }

        return $result;
    }

    /**
     * Find or Create Social
     *
     * Method normally called during OAuth login to create a new or
     * update an existing user.
     * @param  Laravel\Socialite\Contracts\User $social_user
     * @return static
     */
    public function findOrCreateSocial(SocialUser $social_user)
    {
        $fields = $this->mapSocialFields($social_user);
        $fields['email'] = $social_user->email;

        if ($model = $this->model->where('email', $fields['email'])->first()) {
            $user = $this->make($model);
            $user->update($fields);
        } else {
            $user = $this->create($fields);
            $user->afterSocialCreate($social_user);
        }

        return $user;
    }

    /**
     * Map Social Fields
     *
     * Map social fields to DB fields. Possible fields are:
     * id, nickname, name, email, avatar, token, and (for OAuth1)
     * tokenSecret.
     * @param  Laravel\Socialite\Contracts\User $social_user
     * @return array
     */
    protected function mapSocialFields($social_user)
    {
        return [];
    }

    /**
     * After Social Create
     *
     * Perform additional actions on the new user. Remember that
     * $this will be the repository for the new user and $social_user
     * will be the same Socialite User object passed to mapSocialFields.
     * @param  Laravel\Socialite\Contracts\User $social_user
     * @return void
     */
    protected function afterSocialCreate($social_user)
    {
        // Nothing to do
    }
}
